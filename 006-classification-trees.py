from sklearn import tree
import pandas
from sklearn.tree import DecisionTreeClassifier, export_graphviz
import numpy
from sklearn import preprocessing

#no need to preprocess our data
#df = pandas.read_csv('C:/Udemy/SKLEARN-Python/001_customer.csv',
#                     index_col=False, header=0);
#Group    = df["Group"].values
#Features = df[["F1","F2","F3","F4","F5"]].values


#clf = tree.DecisionTreeClassifier(max_depth  = 3 )
#clf = clf.fit(Features,Group)
#clf.predict([[1, 1 ,1 , 2, 2]])


#print(clf.score(Features,Group))

#with open('C:/Udemy/SKLEARN-Python/xcustomer2.dot', 'w') as dotfile:
#    export_graphviz(
#        clf,
#        dotfile,
#        feature_names=["F1","F2","F3","F4","F5"])

#cd C:\Program Files (x86)\Graphviz2.38\bin
#dot.exe -Tpng C:\Udemy\SKLEARN-Python\xcustomer2.dot  -o C:\Udemy\SKLEARN-Python\output.png

#####################################################################
#Tree regression#####################################################

dfx = pandas.read_csv('C:/Users/Johar/Desktop/Python - AI/Scikit Learn/logins.csv',
                     index_col=False, header=0);
Websiteconfig    = dfx["WebsiteConfig"].values
Logins           = dfx["Logins"].values
Traffic_inK      = dfx["Traffic-inK"].values
le = preprocessing.LabelEncoder()
le.fit(Websiteconfig)
Websiteconfig    = le.transform(Websiteconfig)

p = numpy.transpose(numpy.vstack((Websiteconfig,Traffic_inK)))

clf = tree.DecisionTreeRegressor(max_depth  = 5)
clf = clf.fit(p, Logins)
clf.score(p,Logins)

with open('C:/Udemy/SKLEARN-Python/xlogins.dot', 'w') as dotfile:
    export_graphviz(
        clf,
       dotfile,
        feature_names=["TypeB","TrafficK"])
